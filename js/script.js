const btn = document.getElementById('main-btn');
const tableWrapper = document.querySelector('.table-wrapper');
const table = document.createElement('table');

btn.addEventListener('click', (e) => {
    clearTable();
    table.className = 'main-table';
    const row = document.getElementById('input-row');
    const column = document.getElementById('input-column');
    for (let i = 0; i < row.value; i++) {
        let tr = document.createElement('tr');
        for (let j = 0; j < column.value; j++) {
            let td = document.createElement('td');
            td.innerHTML = '';
            tr.appendChild(td);
        }
        table.appendChild(tr);
    }
    tableWrapper.appendChild(table);
});

const clearTable = () => {
    let tableSelector = document.querySelector(".main-table");
    if (tableSelector !== null) {
        while (tableSelector.hasChildNodes()) {
            tableSelector.removeChild(tableSelector.lastChild);
        }
    }
}